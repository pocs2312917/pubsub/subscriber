import os

import datetime
import time
import platform

from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
from google.cloud import logging
from cpu_load_generator import load_single_core, load_all_cores, from_profile

from google.oauth2 import service_account

from dotenv import load_dotenv
load_dotenv()

PROJECT_ID = os.getenv('PROJECT_ID')
PUBSUB_SUBSCRIPTION = os.getenv('PUBSUB_SUBSCRIPTION')
NUM_MESSAGES = int(os.getenv('NUM_MESSAGES'))
PROCCESING_FAKE_TIME = int(os.getenv('PROCCESING_FAKE_TIME'))
CPU_USAGE_LOAD = float(os.getenv('CPU_USAGE_LOAD'))

credentials = service_account.Credentials.from_service_account_file('./sa.json')

subscriber = pubsub_v1.SubscriberClient(credentials=credentials)
subscription_path = subscriber.subscription_path(PROJECT_ID, PUBSUB_SUBSCRIPTION)

logging_client = logging.Client(credentials=credentials)
logger = logging_client.logger(name='gke-pubsub-consumer')


def callback(message) -> None:
    """Process received message"""
    logger.log_struct(
        {
            "message": "[{0}] Processing [{2}][{3}]: {1}".format(datetime.datetime.now(), message.message_id, PUBSUB_SUBSCRIPTION, platform.node())
        },
        severity="INFO"
    )
    #time.sleep(PROCCESING_FAKE_TIME)
    load_single_core(core_num=0, duration_s=PROCCESING_FAKE_TIME, target_load=CPU_USAGE_LOAD)
    logger.log_struct(
        {
            "message": "[{0}] Processed [{2}][{3}]: {1}".format(datetime.datetime.now(), message.message_id, PUBSUB_SUBSCRIPTION, platform.node())
        },
        severity="INFO"
    )
    message.ack()


# Limit the subscriber to only have ten outstanding messages at a time.
flow_control = pubsub_v1.types.FlowControl(max_messages=NUM_MESSAGES)

streaming_pull_future = subscriber.subscribe(
    subscription_path, callback=callback, flow_control=flow_control
)

logger.log_struct(
    {
        "message": f'Listening for messages on {subscription_path} from {platform.node()}'
    },
    severity="INFO"
)

# Wrap subscriber in a 'with' block to automatically call close() when done.
with subscriber:
    try:
        # set timeout to 5 minutes
        streaming_pull_future.result()
        #streaming_pull_future.result()
    except TimeoutError:
        streaming_pull_future.cancel()  # Trigger the shutdown.